;; This file is part of racket-upi - Racket library inspired by UNIX tools.
;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-upi is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-upi is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-upi.  If not, see <https://www.gnu.org/licenses/>.


#lang typed/racket/base

;; NOTICE: In Racket version 8.5: following are unbound
(require/typed racket/file
  [file->bytes  (-> Path-String Bytes)]
  [file->lines  (-> Path-String (Listof String))]
  [file->string (-> Path-String String)])

(provide (all-defined-out))


(define-type WC-Switch
  (U 'bytes 'lines 'words))

(define-type WC-Table
  (Immutable-HashTable WC-Switch Nonnegative-Integer))


(define (wc [switch : WC-Switch] [file-path : Path-String])
        : Exact-Nonnegative-Integer
  (length (case switch
            [(bytes) (bytes->list (file->bytes file-path))]
            [(lines) (file->lines file-path)]
            [(words) (regexp-split #rx"\\ |\n" (file->string file-path))])))

(define (wc-stats [file-path : Path-String]) : WC-Table
  (hash 'bytes (wc 'bytes file-path)
        'lines (wc 'lines file-path)
        'words (wc 'words file-path)))
