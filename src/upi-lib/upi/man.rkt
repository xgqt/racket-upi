;; This file is part of racket-upi - Racket library inspired by UNIX tools.
;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-upi is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-upi is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-upi.  If not, see <https://www.gnu.org/licenses/>.


#lang typed/racket/base

(require (only-in threading ~>>)
         "basename.rkt"
         "split-path-env.rkt")

(provide (all-defined-out))


(define-type Manpage-Name
  (U Regexp String))

(define-type Manpage-Lang
  (Option String))

(define-type Manpage-Page
  (U Number String))


(define default-man-page : (Parameterof Manpage-Page)
  (make-parameter 1))


(define (man-paths) : (Listof String)
  (split-path-env (or (getenv "MANPATH")
                      (error 'man "MANPATH is not set"))))

(define (man [manpage-name : Manpage-Name]
             #:lang [lang : Manpage-Lang #false]
             #:page [page : Manpage-Page (default-man-page)])
        : (Listof Path)
  (for/lists ([lsts : (Listof (Listof Path))]
              #:result (apply append lsts))
             ([man-path (man-paths)])
    : (Listof Path)
    (let ([page-path
           (~>> page
                (format "man~a")
                (build-path man-path (or lang 'same)))]
          [manpage-regexp
           (~>> page
                (format "~a\\.~a.*" manpage-name)  ; needed for regexping
                regexp)])
      (cond
        [(directory-exists? page-path)
         (for/list ([pth (directory-list page-path #:build? #true)]
                    #:when (and (path? pth)
                                (~>> pth
                                     basename
                                     (regexp-match-exact? manpage-regexp))))
           : (Listof Path)
           pth)]
        [else
         '()]))))

(define (man/any [manpage-name : Manpage-Name]
                 #:lang [lang : Manpage-Lang #false])
        : (Listof Path)
  (for/lists ([lsts : (Listof (Listof Path))]
              #:result (apply append lsts))
             ([page (in-range 1 10)])  ; TODO: Consider other weird page names.
    : (Listof Path)
    (man manpage-name #:lang lang #:page page)))

(define (man/first [manpage-name : Manpage-Name]
                   #:lang [lang : Manpage-Lang #false])
        : (Option Path)
  (let ([pages
         (man/any manpage-name #:lang lang)])
    (and (not (null? pages))
         (car pages))))
