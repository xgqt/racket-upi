#lang info


(define pkg-desc "Racket library inspired by UNIX tools. Documentation.")

(define version "0.3")

(define pkg-authors '(xgqt))

(define license 'GPL-2.0-or-later)

(define collection 'multi)

(define deps
  '("base"))

(define build-deps
  '("racket-doc"
    "scribble-lib"
    "ziptie-git"
    "upi-lib"))
