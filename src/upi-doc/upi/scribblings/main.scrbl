;; This file is part of racket-upi - Racket library inspired by UNIX tools.
;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-upi is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-upi is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-upi.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require ziptie/git/hash
          upi/version)


@(define upstream-tree
   "https://gitlab.com/xgqt/racket-upi/-/tree/")


@(define (important str)
   (larger (bold str)))


@title[#:tag "upi"]{Racket-UPI}

@author[@author+email["Maciej Barć" "xgqt@riseup.net"]]


@important{U}nix tools inspired @important{P}rocedure @important{I}nterface
for Racket.

Racket-UPI is a Racket library inspired by miscellaneous UNIX tools,
primarily @link["https://www.gnu.org/software/coreutils/"]{GNU coreutils}
and @link["https://busybox.net/"]{BusyBox}.

Version: @link[@(string-append upstream-tree @VERSION)]{@important{@VERSION}},
commit hash:
@(let ([git-hash (git-get-hash #:short? #t)])
   (case git-hash
     [("N/A")
      (displayln "[WARNING] Not inside a git repository!")
      git-hash]
     [else
      (link (string-append upstream-tree git-hash) (important git-hash))]))


@table-of-contents[]

@include-section{basename.scrbl}
@include-section{cp.scrbl}
@include-section{dirname.scrbl}
@include-section{dirnames.scrbl}
@include-section{realpath.scrbl}
@include-section{rm.scrbl}
@include-section{wget.scrbl}
@include-section{which.scrbl}

@index-section[]
