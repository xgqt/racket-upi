#+TITLE: Wanted

#+STARTUP: showall


* awk

  - Too big?
  - Maybe "needs" more features, like:
    - importing racket code

* clear

  - Better to include in repository like "ncurses"?

* cut

* date

  - Especially the date format-string!

* grep

* pipe "|"

  - "|" is special in Racket...
    #+BEGIN_SRC racket
      ;; (define | 1)  => hangs, waits for terminating "|"
      (define \| 1)
      \|  ; => 1
    #+END_SRC
  - Maybe follow the system spirit and make it a pipe between 2 ~process~ calls?

* sed
