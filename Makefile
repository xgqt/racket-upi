MAKE		?= make
RACKET		:= racket
RACO		:= raco
SCRIBBLE	:= $(RACO) scribble
SH			:= sh

PWD         ?= $(shell pwd)
PUBLIC      := $(PWD)/public
SCRIPTS     := $(PWD)/scripts
SRC         := $(PWD)/src


.PHONY: all
all: compile

src-%:
	cd $(SRC) && $(MAKE) DEPS-FLAGS=" --no-pkg-deps " $(*)

.PHONY: clean-public
clean-public:
	if [ -d $(PUBLIC) ] ; then  rm -r $(PUBLIC) ; fi

.PHONY: clean
clean: src-clean clean-public

.PHONY: compile
compile: src-compile

.PHONY: install
install: src-install

.PHONY: setup
setup: src-setup

.PHONY: test-unit
test-unit:
	$(SH) $(SCRIPTS)/test.sh unit

.PHONY: test-integration
test-integration:
	$(SH) $(SCRIPTS)/test.sh integration

.PHONY: test
test: test-unit test-integration

.PHONY: test-deps
test-deps:
	$(MAKE) -C $(SRC) setup

.PHONY: test-full
test-full: test-deps src-test test

.PHONY: remove
remove: src-remove

$(PUBLIC):
	$(SH) $(SCRIPTS)/public.sh

.PHONY: public
public:
	$(MAKE) -B $(PUBLIC)
